
public class Order {
private int qtyOrdered;
	
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered > MAX_NUMBERS_ORDERED) return false;
		else {
			itemsOrdered[qtyOrdered] = disc;
			this.qtyOrdered ++;
			return true;
		}
	}
	public boolean removeDigitalVideoDisc(DigitalVideoDisc disc) { 
		int c, i;
		boolean result = false;
		for( c = i = 0; i < this.qtyOrdered ; i++) {
			if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
				this.itemsOrdered[c] = this.itemsOrdered[i];
				c++;
			}
		}
		if(this.qtyOrdered != c) {
			this.setQtyOrdered(c);
			result = true;
		}
		return result;
		
	 }

	public float totalCost() {
		float totalCost = 0;
		int i;
		for(i = 0; i < this.qtyOrdered; i++) {
			totalCost += itemsOrdered[i].getCost();
		}
		return totalCost;
	}
	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animaion");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		if(anOrder.addDigitalVideoDisc(dvd1)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		if(anOrder.addDigitalVideoDisc(dvd2)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animaion");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		if(anOrder.addDigitalVideoDisc(dvd3)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		System.out.print("Total cost is: ");
		System.out.println(anOrder.totalCost());
	}

}

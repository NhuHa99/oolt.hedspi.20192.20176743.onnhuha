package week4;

import java.util.Date;

public class orders {
	public static final int MAX_NUMBERS_ORDERED = 10;
	  public static final int MAX_LIMITED_ORDERS = 5;
	  private static int nbOrders;
	  private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	  private int qtyOrdered;
	  private Date dateOrdered;

	  public void orders() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }

	  public int getQtyOrdered() {
	    return qtyOrdered;
	  }

	  public void setQtyOrdered(int qtyOrdered) {
	    this.qtyOrdered = qtyOrdered;
	    
	  }

	  public Date getDateOrdered() {
	    return dateOrdered;
	  }

	  public void setDateOrdered() {
	    dateOrdered = new Date();;
	  }

	 

	  public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
			if(this.qtyOrdered > MAX_NUMBERS_ORDERED) return false;
			else {
				itemsOrdered[qtyOrdered] = disc;
				this.qtyOrdered ++;
				return true;
			}
	  }
	  public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		    if (qtyOrdered + dvdList.length < MAX_NUMBERS_ORDERED ) {
		      for (int i = 0; i < dvdList.length; i++) {
		        itemsOrdered[qtyOrdered] = dvdList[i];
		        qtyOrdered++;
		        return ;
		      }
		    } else {
		      System.out.println("The order is almost full!");
		    }
		  }

	  public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
	    if (qtyOrdered + 2 < 10) {
	      itemsOrdered[qtyOrdered] = dvd1;
	      qtyOrdered++;
	      itemsOrdered[qtyOrdered] = dvd2;
	      qtyOrdered++;
	      System.out.println("The disc has been added!");
	    } else {
	      System.out.println("The order is almost full!");
	    }
	  }

	  public boolean removeDigitalVideoDisc(DigitalVideoDisc disc) { 
			int c, i;
			boolean result = false;
			for( c = i = 0; i < this.qtyOrdered ; i++) {
				if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
					this.itemsOrdered[c] = this.itemsOrdered[i];
					c++;
				}
			}
			if(this.qtyOrdered != c) {
				this.setQtyOrdered(c);
				result = true;
			}
			return result;
			
		 }

	  public float totalCost() {
			float totalCost = 0;
			int i;
			for(i = 0; i < this.qtyOrdered; i++) {
				totalCost += itemsOrdered[i].getCost();
			}
			return totalCost;
		}
	  public void printOrder() {
		    System.out.println("*************** Order********************");
		    System.out.println("Date: " + dateOrdered);
		    System.out.println("Ordered items:");
		    for (int i = 0; i < qtyOrdered; i++) {
		      System.out.println(i + 1 + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
		    }
		    System.out.println("Total cost: " + this.totalCost());
		    System.out.println("*******************************************");
		  }

}

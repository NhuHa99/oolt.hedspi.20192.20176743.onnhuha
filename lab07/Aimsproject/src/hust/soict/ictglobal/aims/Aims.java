package hust.soict.ictglobal;
import hust.soict.ictglobal.media.*;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.Order.*;

public class Aims {
	private static ArrayList<Order> listOrder = new ArrayList<Order>();
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4: ");
		}
	
	public static void main(String[] args) {
		MemoryDaemon md = new MemoryDaemon();
	    Thread thread = new Thread(md);
	    thread.setDaemon(true);
		Scanner keyBoard = new Scanner(System.in);
		int c = 0;
		Order order=null;
		do {
			 showMenu();
			 c = Integer.parseInt(keyBoard.nextLine());
			 switch(c) {
			 
			 	case 1:{
			 	    order= new Order();
			 	    listOrder.add(order);
					System.out.println("Created new item successfully");
			 	}break;
			 
			 	case 2:{
					int a = 0;
			 		System.out.println("List: ");
			 		order.printListOrdered();
			 		do {
			 		System.out.println("1.Book - 2.CompactDisc - 3.DigitalVideoDisc .please choose");
			 		a = Integer.parseInt(keyBoard.nextLine());
			 		switch (a) {
			 		case 1:{
						Book b=InputBook();
						listOrder.get(listOrder.size()-1).addMedia(b);
			 		}break;
			 		case 2:{
						CompactDisc cd=InputCompactDisc();
						listOrder.get(listOrder.size()-1).addMedia(cd);
			    		System.out.print("press 1 to execute:");
			    		int chon1 = Integer.parseInt(keyBoard.nextLine());
			    		if(chon1==1){
			    				cd.play();
			    			}
			 		}break;
					case 3:{
						DigitalVideoDisc dvd= InputDVD();
						listOrder.get(listOrder.size()-1).addMedia(dvd);
						System.out.print("press 1 to execute:");
			    		int chon2 = Integer.parseInt(keyBoard.nextLine());
			    		if(chon2==1){
			    				dvd.play();
			    			}
					}break;
					default:
						break;
						}
					}while(a == 1 || a==2 || a==3);
			 	}break;
			 	case 3:{
			 		System.out.println();
					System.out.println("Delete item by id");
					System.out.print("Enter the index you want to delete: ");
					int id=Integer.parseInt(keyBoard.nextLine());
					order.removeMediaByID(id);
			 	}break;
			 	case 4:{
			 		System.out.println("items list of order");
			 		order.printListOrdered();
			 	}break;
			 	default: {
			 		System.out.println("");
			 	}break;
			 }	 
		}while(c == 1 || c==2 || c==3 || c==4);
		
	}

	

}

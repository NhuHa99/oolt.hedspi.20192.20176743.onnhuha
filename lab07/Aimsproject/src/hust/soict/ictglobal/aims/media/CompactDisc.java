package hust.soict.ictglobal.media;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable {
	private String  artist;
	private int  length;
	private List<Track> tracks = new ArrayList<Track>();
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public CompactDisc() {
		
	}
	public void addTrack(Track track) {
		if(tracks.contains(track)==false) {
			tracks.add(track);
			System.out.println(track.getTitle() +"a has been added");
		}else
			
			System.out.println( track.getTitle()+"already exists");
		
	}
	public void removeTrack(Track track) {
		if(tracks.contains(track)==true) {
			tracks.remove(track);
			System.out.println( track.getTitle()+"has been deleted successfully");
		}else
			System.out.println(track.getTitle() +"already exists ");
	}
	public int getLength() {
		int sum = 0;
		for(Track i:tracks)
		{
			sum = sum + i.getLength();
		}
		return sum ;
		
	}
	public static CompactDisc InputCompactDisc(){
		CompactDisc cd = new CompactDisc();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		cd.setTitle(sc.nextLine());
		System.out.print("Category: ");
		cd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		cd.setDirector(sc.nextLine());
		System.out.print("Artist: ");
		cd.setArtist(sc.nextLine());
		System.out.print("Cost: ");
		cd.setCost(Float.parseFloat(sc.nextLine()));
		int t=1;
		int i=1;
		while(t==1){
			Track track = new Track();
			System.out.print("add track with ");
			System.out.print("Title is"+ i +": ");
			track.setTitle(sc.nextLine());
			System.out.print("Length is "+ i +": ");
			track.setLength(Integer.parseInt(sc.nextLine()));
			cd.addTrack(track);
			i++;
			System.out.print("neu muon them track hay nhap 1. ban nhap :");
			t= Integer.parseInt(sc.nextLine());		
		}
		return cd;
	}
	public void play() {
		System.out.println("Artist: "+this.getArtist());
    	System.out.println("CompactDisc length: "+this.getLength());
    	for(Track tr: tracks){
    		tr.play();
    	}
    }


}

package hust.soict.ictglobal.media;

import java.util.Scanner;


public class DigitalVideoDisc extends Disc implements Playable {
	public DigitalVideoDisc(String title, String category, String director,float cost, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	public DigitalVideoDisc(){
	}
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean search(String title) {
		title= title.trim();
    	String[] token = title.split(" ");
    	boolean flag=true;
    	for(int i=0;i<token.length;i++){
    		if(title.contains(token[i])==false){
    			return false;
    		}
    	}
    	return true;
	}
	public static DigitalVideoDisc InputDVD(){
		DigitalVideoDisc dvd = new DigitalVideoDisc();
		Scanner d = new Scanner(System.in);
		System.out.print("Title: ");
		dvd.setTitle(d.nextLine());
		System.out.print("Category: ");
		dvd.setCategory(d.nextLine());
		System.out.print("Director: ");
		dvd.setDirector(d.nextLine());
		System.out.print("Length: ");
		dvd.setLength(Integer.parseInt(d.nextLine()));
		System.out.print("Cost: ");
		dvd.setCost(Float.parseFloat(d.nextLine()));
		return dvd;
	}
	public void play() {
		System.out.println("Playing DVD: "+this.getTitle());
    	System.out.println("DVD length: "+this.getLength());
		
	}

}

package hust.soict.ictglobal.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Book extends Media {
	public Book(String title, String category, float cost) {
		super(title, category,cost);
	}
	public Book() {
	}
	private String title;
	private String category;
	private float cost;
	private List<String> authors = new ArrayList<String>();
	
	/*public void addAuthor(String auth) {
		if(authors.contains(auth)==false)
		{
			this.authors.add(auth);
			System.out.println(auth+"a has been added");
		}else {
			System.out.println(auth +"already exists ");
		}
	}
	public void removeAuthor(String auth) {
		if(authors.contains(auth)==false) {
			System.out.println(auth +"already exists");
		}else {
			this.authors.remove(auth);
			System.out.println(auth +"has been deleted successfully");
		}
	}*/
	public void addAuthor(String authorName) {
	    if (authors.contains(authorName)) {
	      System.out.println("This author is already exists!");
	    } else {
	      authors.add(authorName);
	      System.out.println("Add success!");
	    }
	  }

	  public void removeAuthor(String authorName) {
	    if (authors.remove(authorName)) {
	      System.out.println("Remove success!");
	    } else {
	      System.out.println("This author is not exists!");
	    }
	  }	
	  public static Book InputBook(){
			Book book= new Book();
			Scanner b = new Scanner(System.in);
			System.out.print("Title: ");
			book.setTitle(b.nextLine());
			System.out.print("Category: ");
			book.setCategory(b.nextLine());
			System.out.print("Cost: ");
			book.setCost(Float.parseFloat(b.nextLine()));
			int t=1;
			int i=1;
			while(t==1){
				System.out.print("Author "+i+": ");
				String author= b.nextLine();
				book.addAuthor(author);
				i++;	
				System.out.print("Nhap vao 1 neu ban muon add. ban nhap : ");
				t= Integer.parseInt(b.nextLine());
				}
			return book;
		}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

}
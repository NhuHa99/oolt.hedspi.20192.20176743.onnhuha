package hust.soict.ictglobal.media;



public class Disc extends Media {
	private int length;
    private String director;
    
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = (int) length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	public Disc(){
		
	}
	public Disc(String title, String category, float cost, int id) {
		super(title, category, cost);
		this.id=id;
	}

	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		
	}
	
	


}

package week4;

public class Media {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String title;
	private String category;
	private float cost;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	public boolean search(String title){
    	title= title.trim();
    	String[] token = title.split(" ");
    	boolean flag=true;
    	for(int i=0;i<token.length;i++){
    		if(title.contains(token[i])==false){
    			return false;
    		}
    	}
    	return true;
    }


}

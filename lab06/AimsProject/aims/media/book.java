package week4;
import java.util.ArrayList;
import java.util.List;

public class book extends Media{
	private List<String> authors = new ArrayList<String>();

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
	    if (authors.contains(authorName)) {
	      System.out.println("This author is already exists!");
	    } else {
	      authors.add(authorName);
	      System.out.println("Add success!");
	    }
	  }

	  public void removeAuthor(String authorName) {
	    if (authors.remove(authorName)) {
	      System.out.println("Remove success!");
	    } else {
	      System.out.println("This author is not exists!");
	    }
	  }	
}

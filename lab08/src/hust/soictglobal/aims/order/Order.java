package hust.soictglobal.aims.order;

import hust.soictglobal.aims.media.*;
import java.util.*;
import java.util.Date;

public class Order {
	public static final int MAX_NUMBER_ORDERED =10;
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders =0;
    private List<Media> itemsOrdered=  new ArrayList<Media>();
	
	private Date dateOrdered;
	public Order() {
	    if (nbOrders == MAX_LIMITED_ORDERS) {
	      System.out.println("Max limited orders in day");
	      return;
	    }
	    this.setDateOrdered();
	    nbOrders++;
	  }

    public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	  public void setDateOrdered() {
		    dateOrdered = new Date();;
		  }


	   public void addMedia(Media media) {
		    if (itemsOrdered.size() < MAX_NUMBER_ORDERED) {
		      itemsOrdered.add(media);
		      System.out.println("The media has been added!");
		    } else {
		      System.out.println("The order is almost full!");
		    }
		  }
	   public  void removeMedia(int id) {
		    for (int i = 0; i < itemsOrdered.size(); i++) {
		      if (itemsOrdered.get(i).getId() == id) {
		        itemsOrdered.remove(i);
		        System.out.println("The disc has been removed!");
		        return;
		      }
		    }
		    System.out.println("Media is not in order!");
		   
		  }
	  public void removeMedia(Media media) {
		    if (itemsOrdered.remove(media)) {
		      System.out.println("The disc has been removed!"+ media.getTitle());
		    } else {
		      System.out.println("Media is not in order!");
		    }
		  }
	

	public float totalCost() {
		float total = 0;
		
		for(int i = 0; i < itemsOrdered.size(); i++) {
			total += itemsOrdered.get(i).getCost();
		}
		return total;
	}
	public boolean Search(String title) {
		for(int a = 0; a <itemsOrdered.size() ; a++) {
			String tmp= new String();
			tmp= itemsOrdered.get(a).getTitle();
			if ( title.equalsIgnoreCase(tmp)) {
				System.out.println(tmp);
				return true;
				}else {
					if(tmp.contains(title)) {
						System.out.println(tmp);
						return true;
					}
				}
	}
		return false;
	}
public Media  getALuckyItem() {
	    int ramdom = (int)(itemsOrdered.size() * Math.random());
	    itemsOrdered.get(ramdom).setCost(0);
	    return itemsOrdered.get(ramdom);
	  }
   
public void printOrder() {
	System.out.println("****************-------ORDER------******************");
    System.out.println(" Show Date: " + dateOrdered);
    System.out.println("Show Ordered items:");
    for (int a = 0; a < itemsOrdered.size(); a++) {
      System.out.println(a + 1 + ". DVD - " + itemsOrdered.get(a).getTitle() + " - " + 
    		  itemsOrdered.get(a).getCategory() + " - "  + " - "  + ": " + itemsOrdered.get(a).getCost() + "$");
    }
    System.out.println("Total cost: " + this.totalCost()+"$");
    System.out.println("********----END-----------************");
  }

  }

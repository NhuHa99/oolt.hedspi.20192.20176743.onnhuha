package hust.soictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable {
	  private String artist;
	  private int length;
	  private ArrayList<Track> tracks = new ArrayList<Track>();
	  public CompactDisc() {
			
		}
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	
	public void addTrack(Track track) {
		if(tracks.contains(track)==false) {
			tracks.add(track);
			System.out.println(track.getTitle() +"a has been added");
		}else
			
			System.out.println( track.getTitle()+"already exists");
		
	}
    public void removeTrack(Track track) {
		if(tracks.contains(track)==true) {
			tracks.remove(track);
			System.out.println( track.getTitle()+"has been deleted successfully");
		}else
			System.out.println(track.getTitle() +"already exists ");
	}
    public int getLength() {
        int length = 0;
        for (int a = 0; a < tracks.size(); a++) {
          length += tracks.get(a).getLength();
        }
        return length;
      }

  
    public void play() {
		System.out.println("Artist: "+this.getArtist());
    	System.out.println("CompactDisc length: "+this.getLength());
    	for(Track tr: tracks){
    		tr.play();
    	}
    }
 
    
    public int compareTo(Object obj) {
    	 CompactDisc CD = (CompactDisc) obj;

    	 if (tracks.size() > CD.tracks.size()) return 1;
    	 else if (tracks.size() < CD.tracks.size()) return -1;
    	 else {
    	    if (this.getLength() > CD.getLength()) return 1;
    	    else if (this.getLength() < CD.getLength()) return -1;
    	    return 0;
    	    }
    	}
    }